FROM alpine:latest
ARG branch=master

WORKDIR /src

RUN apk --no-cache --virtual .fontsinstaller add msttcorefonts-installer fontconfig \
  && update-ms-fonts \
  && fc-cache -f \
  && apk del .fontsinstaller \
  && rm -f /var/cache/apk/*
ENV GDFONTPATH=/usr/share/fonts/truetype/msttcorefonts/

# Install gnuplot from source, to make sure we do have GD support etc.
# Qt is failing on gnuplot 6.1
RUN apk --update add libgd libpng libjpeg libjpeg-turbo libgcc libwebp libwebpmux cairo pango lua readline libcaca \
 && apk add --virtual .gnuplotbuild git gcc g++ libtool make automake autoconf cairo-dev pango-dev gd-dev lua-dev readline-dev libpng-dev libjpeg-turbo-dev libwebp-dev libcaca-dev \
 && mkdir -p /src && cd /src \
 && git clone -b $branch --depth 1 https://git.code.sf.net/p/gnuplot/gnuplot-main gnuplot-$branch  \
 && cd gnuplot-$branch \
 && ./prepare \
 && ./configure --without-qt --with-caca \
 && make -j $(nproc) \
 && make install \
 && cd / && rm -rf /src/gnuplot-$branch \
 && apk del .gnuplotbuild \
 && mkdir -p /work \
 && rm -f /var/cache/apk/*

WORKDIR /work

ENTRYPOINT ["/usr/local/bin/gnuplot"]
CMD ["--help"]
#ENTRYPOINT ["/bin/sh","-c","sleep 1221221"]

