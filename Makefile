BRANCH ?= master
PATCH ?=

all: build
	$(info Use with:)
	$(info docker run --rm --volume .:/work/ -w /work/ gnuplot:$(BRANCH) <options>)

build:
	docker build -t gnuplot:$(BRANCH)$(PATCH) --build-arg branch=$(BRANCH) .
	docker run gnuplot:$(BRANCH)$(PATCH) gnuplot --version
