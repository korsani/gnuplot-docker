# gnuplot-docker

Look for the latest `gnuplot`? Search no more, build it yourself!

## Build

`gnuplot` is built with webp, pango, cairo, caca.

But WITHOUT Qt, wxt, lua/TikZ

Run

	# <tag> is defaulted to 'master'
	make BRANCH=<tag>

Or

	docker build -t gnuplot:latest --build-arg branch=latest .

## Run

Go into the directory "your-plot.plot" resides, then:

	docker run --rm --volume .:/work/ -w /work/ gnuplot:<tag> "your-plot.plot"

## Examples

I want the latest `gnuplot`

	make
	alias gnuplot-latest="docker run --rm --volume .:/work/ -w /work/ gnuplot:master"
	gnuplot-latest --version

## Credits

Based on the work from Remus Lazar : https://github.com/remuslazar/docker-gnuplot
